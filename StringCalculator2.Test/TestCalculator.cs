﻿using NUnit.Framework;
using System;

namespace StringCalculator2.Test
{
    [TestFixture]
    public class TestCalculator
    {
        private Calculator _calculator;

        [OneTimeSetUp]
        public void Init()
        {
            _calculator = new Calculator();
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Adding_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Add(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Adding_THEN_ReturnThatNumber()
        {
            var expected = 1;
            var actual = _calculator.Add("1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumber_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 4;
            var actual = _calculator.Add("1,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,3,6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewlineAsADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("1,3\n6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;3;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumber_WHEN_Adding_THEN_ThrowException()
        {
            var expected = "Negative not allowed -6, -7";
            var actual = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;3;-6;-7"));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_NumberMoreThanThousand_WHEN_Adding_THEN_IngoreThem()
        {
            var expected = 10;
            var actual = _calculator.Add("//;\n1;3;6;10002");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//;;\n1;;3;;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterMultipleCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[*][.]\n1*3.6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterMultipleCustomADelimiter_WHEN_Adding_THEN_ReturnSum()
        {
            var expected = 10;
            var actual = _calculator.Add("//[**][..]\n1**3..6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_EmptyString_WHEN_Subtrating_THEN_ReturnZero()
        {
            var expected = 0;
            var actual = _calculator.Subtract(string.Empty);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_OneNumber_WHEN_Subtrating_THEN_ReturnThatNumber()
        {
            var expected = -1;
            var actual = _calculator.Subtract("-1");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_TwoNumber_WHEN_Subtrating_THEN_ReturnSum()
        {
            var expected = -4;
            var actual = _calculator.Subtract("1,3");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleNumbers_WHEN_Subtrating_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("1,3,6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NewlineAsADelimiter_WHEN_Subtracting_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("1,3\n6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterCustomADelimiter_WHEN_Subtrating_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("##;\n1;3;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NumbersAboveThousand_WHEN_Subtracting_THEN_ThrowException()
        {
            var expected = "Numbers above a thousand are not allowed 10001, 10234";
            var actual = Assert.Throws<Exception>(() => _calculator.Subtract("##;\n1;3;10001;10234"));

            Assert.AreEqual(expected, actual.Message);
        }

        [Test]
        public void GIVEN_MultipleCharacterCustomADelimiter_WHEN_Subtrating_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("##;;\n1;;3;;6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_SingleCharacterMultipleCustomADelimiter_WHEN_Subtracting_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("##[*][.]\n1*3.6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_MultipleCharacterMultipleCustomADelimiter_WHEN_Subtracting_THEN_ReturnSum()
        {
            var expected = -10;
            var actual = _calculator.Subtract("##[**][..]\n1**3..6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_NegativeNumbers_WHEN_Subtracting_CorrectThemAndReturnSum()
        {
            var expected = -12;
            var actual = _calculator.Subtract("##[**]\n10**-2");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_AcceptedAlphabets_WHEN_Subtracting_ReturnSum()
        {
            var expected = -1;
            var actual = _calculator.Subtract("##[**]\na**b");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_UnacceptedAlphabets_WHEN_Subtracting_IgnoreThem()
        {
            var expected = -1;
            var actual = _calculator.Subtract("##[**]\na**b**z");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_CustomDelimiterSplitterForSingleDelimiter_WHEN_Subtracting_ReturnSum()
        {
            var expected = -12;
            var actual = _calculator.Subtract("<(>)##(*)\n2*4*6");

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GIVEN_CustomDelimiterSplitterForMultipleDelimiter_WHEN_Subtracting_ReturnSum()
        {
            var expected = -12;
            var actual = _calculator.Subtract("<(>)##(*)(.)\n2*4.6");

            Assert.AreEqual(expected, actual);
        }
    }
}

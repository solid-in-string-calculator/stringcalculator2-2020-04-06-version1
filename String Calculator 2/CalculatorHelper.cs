﻿using System;
using System.Collections.Generic;

namespace String_Calculator_2
{
    public class CalculatorHelper
    {
        private string _customDelimiterId;
        private readonly string _newline = "\n";
        private string[] _delimiterSplitter;

        public CalculatorHelper(string operation, string numbers)
        {
            SetCustomDelmiterId(operation);
            SetDelimiterSplitter(numbers);
        }

        public void SetCustomDelmiterId(string operation)
        {
            if (operation.Equals("+"))
            {
                _customDelimiterId = "//";
                return;
            }

            _customDelimiterId = "##";
        }

        public void SetDelimiterSplitter(string numbers)
        {
            var customSplitterIdentifier = new[] { "<", ">" };

            if (numbers.StartsWith("<"))
            {
                _delimiterSplitter = new[] { numbers.Substring(numbers.IndexOf(customSplitterIdentifier[0]) + 1, numbers.IndexOf(customSplitterIdentifier[1]) - 1), numbers.Substring(numbers.IndexOf(">") + 1, numbers.IndexOf(_customDelimiterId) - 3) };
                return;
            }

            _delimiterSplitter = new[] { "[", "]" };
        }

        public string[] GetDelimiters(string numbers)
        {
            var multipleCustomDelimitersId = _customDelimiterId + _delimiterSplitter[0];
            var multipleCustomDelimitersSeperator = _delimiterSplitter[1] + _newline;
            var multipleCustomDelimitersSplitter = _delimiterSplitter[1] + _delimiterSplitter[0];

            if (numbers.StartsWith(multipleCustomDelimitersId))
            {
                var delimiters = numbers.Substring(numbers.IndexOf(multipleCustomDelimitersId) + multipleCustomDelimitersId.Length, numbers.IndexOf(multipleCustomDelimitersSeperator) - (multipleCustomDelimitersSeperator.Length + 1));

                return delimiters.Split(new[] { multipleCustomDelimitersSplitter }, StringSplitOptions.RemoveEmptyEntries);
            }
            else if (numbers.StartsWith(_customDelimiterId))
            {
                return new[] { numbers.Substring(numbers.IndexOf(_customDelimiterId) + _customDelimiterId.Length, numbers.IndexOf(_newline) - (_newline.Length + 1)) };
            }

            return new[] { ",", "\n" };
        }

        public string[] GetNumbersArray(string numbersSection, string[] delimiters)
        {
            string[] acceptedAlphabets = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j" };

            for (int i = 0; i < acceptedAlphabets.Length; i++)
            {
                numbersSection = numbersSection.Replace(acceptedAlphabets[i], i.ToString());
            }

            return numbersSection.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
        }

        public List<int> GetNumbers(string[] numbersArray)
        {
            List<int> numbersList = new List<int>();

            foreach (var num in numbersArray)
            {
                if (int.TryParse(num, out int number))
                {
                    numbersList.Add(number);
                }
            }

            return numbersList;
        }

        public void ValidateNumbersAboveThousand(List<int> numbersList)
        {
            List<string> numbersAboveThousand = new List<string>();

            foreach (var number in numbersList)
            {
                if (number > 1000)
                {
                    numbersAboveThousand.Add(number.ToString());
                }
            }

            if (numbersAboveThousand.Count != 0)
            {
                throw new Exception($"Numbers above a thousand are not allowed {string.Join(", ", numbersAboveThousand)}");
            }
        }

        public void ValidateNegativeNumbers(List<int> numbersList)
        {
            List<string> negativeNumber = new List<string>();

            foreach (var number in numbersList)
            {
                if (number < 0)
                {
                    negativeNumber.Add(number.ToString());
                }
            }

            if (negativeNumber.Count != 0)
            {
                throw new Exception($"Negative not allowed {string.Join(", ", negativeNumber)}");
            }
        }

        public string GetNumbersSection(string numbers)
        {
            if (numbers.StartsWith(_customDelimiterId))
            {
                return numbers.Substring(numbers.IndexOf("\n") + 1);
            }

            return numbers;
        }

        public string[] GetSortedDelimiters(string[] delimiters)
        {
            Array.Sort(delimiters, (x, y) => y.Length.CompareTo(x.Length));

            return delimiters;
        }

        public int GetSum(List<int> numbersList)
        {
            int sum = 0;

            foreach (var number in numbersList)
            {
                if (number < 1001)
                {
                    sum += number;
                }
            }

            return sum;
        }

        public int GetDifferent(List<int> numbersList)
        {
            var different = 0;
            int temp;

            foreach (var number in numbersList)
            {
                temp = number;
                if (temp < 0)
                {
                    temp = -temp;
                }

                different += temp;
            }

            return -different;
        }
    }
}

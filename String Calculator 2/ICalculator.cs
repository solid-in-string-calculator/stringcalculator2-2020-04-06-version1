﻿namespace String_Calculator_2
{
    public interface ICalculator
    {
        int Add(string numbers);

        int Subtract(string numbers);
    }
}

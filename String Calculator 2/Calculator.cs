﻿using String_Calculator_2;
using System;
using System.Collections.Generic;

namespace StringCalculator2
{
    public class Calculator : ICalculator
    {
        private CalculatorHelper _calculatorHelper;

        public int Add(string numbers)
        {
            _calculatorHelper = new CalculatorHelper("+", numbers);

            var numbersSection = _calculatorHelper.GetNumbersSection(numbers);
            var delimiters = _calculatorHelper.GetDelimiters(numbers);
            delimiters = _calculatorHelper.GetSortedDelimiters(delimiters);
            var numbersArray = _calculatorHelper.GetNumbersArray(numbersSection, delimiters);
            List<int> numbersList = _calculatorHelper.GetNumbers(numbersArray);

            _calculatorHelper.ValidateNegativeNumbers(numbersList);

            return _calculatorHelper.GetSum(numbersList);
        }

        public int Subtract(string numbers)
        {
            _calculatorHelper = new CalculatorHelper("-", numbers);

            var numbersSection = _calculatorHelper.GetNumbersSection(numbers);
            var delimiters = _calculatorHelper.GetDelimiters(numbers);
            delimiters = _calculatorHelper.GetSortedDelimiters(delimiters);
            var numberArray = _calculatorHelper.GetNumbersArray(numbersSection, delimiters);
            List<int> numbersList = _calculatorHelper.GetNumbers(numberArray);

            _calculatorHelper.ValidateNumbersAboveThousand(numbersList);

            return _calculatorHelper.GetDifferent(numbersList);
        }
    }
}
